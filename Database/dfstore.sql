-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Des 2022 pada 04.20
-- Versi server: 10.4.22-MariaDB
-- Versi PHP: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dfstore`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `hero_unit`
--

CREATE TABLE `hero_unit` (
  `id` int(11) NOT NULL,
  `label` varchar(100) NOT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `file_foto` text NOT NULL,
  `status_acc` enum('pending','setuju','tolak') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `hero_unit`
--

INSERT INTO `hero_unit` (`id`, `label`, `deskripsi`, `file_foto`, `status_acc`) VALUES
(2, 'label2', 'lorem ipsum 2', 'slider22.jpg', 'setuju'),
(7, 'Gambar Toko Online', 'Ini adalah gambar contoh untuk toko online.', 'slider1.jpg', 'setuju'),
(10, 'Hero Toko Online', 'Ini adalah gambar contoh untuk toko online.', 'Computer_031.jpg', 'setuju'),
(11, 'Hero Toko Online', 'tes', '434109.jpg', 'pending'),
(12, 'CTH Hero Toko Online', 'Contoh', '101.jpg', 'pending'),
(20, 'hero', 'heroooo', 'EDDIE_SKYLINE.jpg', 'setuju');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `id_brg` int(11) NOT NULL,
  `nama_brg` varchar(120) NOT NULL,
  `deskripsi` varchar(225) NOT NULL,
  `kategori` enum('Pakaian Pria','Pakaian Wanita','Pakaian Anak','Elektronik','Kebutuhan Pokok','Kesehatan') NOT NULL,
  `harga` decimal(10,2) NOT NULL,
  `stok` int(4) NOT NULL,
  `gambar` text NOT NULL,
  `status_acc` enum('pending','setuju','tolak') NOT NULL DEFAULT 'pending',
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_barang`
--

INSERT INTO `tbl_barang` (`id_brg`, `nama_brg`, `deskripsi`, `kategori`, `harga`, `stok`, `gambar`, `status_acc`, `created_at`, `update_at`) VALUES
(1, 'Jaket Kulit Adidas Original', 'Jaket kulit asli merek Adidas.', 'Pakaian Pria', '450000.00', 2, 'jaket.jpg', 'setuju', '2022-12-13 11:11:00', '2022-12-13 14:17:26'),
(2, 'Jaket Wanita Bahan Kulit', 'Cocok untuk wanita yang suka memakai jaket kulit ataupun rider.', 'Pakaian Wanita', '1000000.00', 4, 'jaket2.jpg', 'setuju', '2022-12-13 11:11:00', '2022-12-13 11:11:00'),
(5, 'Kentang Beku ', 'Kentang Beku 2.5 kg', 'Pakaian Pria', '65000.00', 20, '48804402.jpg', 'tolak', '2022-12-13 11:11:00', '2022-12-13 21:09:26'),
(6, 'HP iPhone Xr', 'HP iPhone Xr versi ram 3/128', 'Elektronik', '4000000.00', 3, 'iponx.jpg', 'setuju', '2022-12-13 11:11:00', '2022-12-13 11:11:00'),
(7, 'Bola Futsal Specs Original', 'Bola untuk olahraga futsal merk specs berkualitas original dari pabrik.', 'Kesehatan', '100000.00', 9, 'ax2xSBVr2UQt2CcVFAWuv4gJUWZKzS89mwwZbVB2.jpg', 'setuju', '2022-12-13 11:11:00', '2022-12-13 11:11:00'),
(8, 'Game Need For Speed Underground 1 ', 'Game Need For Speed Underground 1 Versi CD', 'Kebutuhan Pokok', '100000.00', 10, '446005.jpg', 'setuju', '2022-12-13 11:11:00', '2022-12-13 11:11:00'),
(9, 'Pakaian Anak Gambar Mobil', 'Pakaian anak bahan halus', 'Pakaian Anak', '500000.00', 5, '687759.jpg', 'setuju', '2022-12-13 11:11:00', '2022-12-13 11:11:00'),
(13, 'Pakaian Anak Gambar Mobil', 'Pakaian anak bahan halus', 'Pakaian Anak', '300000.00', 6, '652823.png', 'setuju', '2022-12-13 11:11:00', '2022-12-13 11:11:00'),
(14, 'Pakaian Anak Gambar Mobil', 'Pakaian anak bahan halus', 'Pakaian Anak', '400000.00', 5, '681958.jpg', 'setuju', '2022-12-13 11:11:00', '2022-12-13 11:11:00'),
(15, 'Tas Wanita', 'Tas untuk Wanita', 'Pakaian Wanita', '450000.00', 7, 'ax2xSBVr2UQt2CcVFAWuv4gJUWZKzS89mwwZbVB22.jpg', 'setuju', '0000-00-00 00:00:00', '2022-12-13 11:28:13'),
(16, 'tes barang', 'barang tes', 'Pakaian Anak', '30000.00', 7, '652820.png', 'pending', '2022-12-13 21:11:51', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_invoice`
--

CREATE TABLE `tbl_invoice` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `tgl_pesan` datetime NOT NULL,
  `batas_bayar` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_invoice`
--

INSERT INTO `tbl_invoice` (`id`, `nama`, `alamat`, `tgl_pesan`, `batas_bayar`) VALUES
(6, 'Azril Delfrian Adi Putra', 'Tabanan, Bali', '2022-12-13 20:10:49', '2022-12-14 20:10:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pesanan`
--

CREATE TABLE `tbl_pesanan` (
  `id` int(11) NOT NULL,
  `id_invoice` int(11) NOT NULL,
  `id_brg` int(11) NOT NULL,
  `nama_brg` varchar(66) NOT NULL,
  `jumlah` int(3) NOT NULL,
  `harga` int(10) NOT NULL,
  `pilihan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_pesanan`
--

INSERT INTO `tbl_pesanan` (`id`, `id_invoice`, `id_brg`, `nama_brg`, `jumlah`, `harga`, `pilihan`) VALUES
(0, 1, 1, 'Jaket Kulit Adidas', 1, 500000, ''),
(0, 1, 2, 'Jaket lorem ipsum', 1, 1000000, ''),
(0, 3, 1, 'Jaket Kulit Adidas', 2, 500000, ''),
(0, 4, 1, 'Jaket Kulit Adidas', 1, 500000, ''),
(0, 4, 5, 'Kentang Beku', 1, 65000, ''),
(0, 5, 5, 'Kentang Beku', 1, 65000, ''),
(0, 5, 1, 'Jaket Kulit Adidas', 1, 500000, ''),
(0, 5, 2, 'Jaket lorem ipsum', 1, 1000000, ''),
(0, 5, 6, 'HP IPONG 99 MAX PRO ', 1, 300000000, ''),
(0, 5, 7, 'Bola Futsal Specs Original', 1, 100000, ''),
(0, 6, 1, 'Jaket Kulit Adidas Original', 1, 450000, ''),
(0, 6, 2, 'Jaket Wanita Bahan Kulit', 1, 1000000, ''),
(0, 6, 6, 'HP iPhone Xr', 1, 4000000, '');

--
-- Trigger `tbl_pesanan`
--
DELIMITER $$
CREATE TRIGGER `pesanan_penjualan` AFTER INSERT ON `tbl_pesanan` FOR EACH ROW BEGIN
	UPDATE tbl_barang SET stok = stok-NEW.jumlah
    WHERE id_brg = NEW.id_brg;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `nama` varchar(66) NOT NULL,
  `username` varchar(66) NOT NULL,
  `password` varchar(66) NOT NULL,
  `role_id` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `nama`, `username`, `password`, `role_id`) VALUES
(1, 'admin', 'admin', '123', 1),
(2, 'staff', 'staff', '123', 2),
(3, 'user', 'user', '321', 3),
(4, 'azril', 'azril', 'azril', 3);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `hero_unit`
--
ALTER TABLE `hero_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`id_brg`);

--
-- Indeks untuk tabel `tbl_invoice`
--
ALTER TABLE `tbl_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `hero_unit`
--
ALTER TABLE `hero_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `id_brg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tbl_invoice`
--
ALTER TABLE `tbl_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
