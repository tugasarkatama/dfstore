<div class="container-fluid">

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <?php foreach($hero as $key => $rows) :?>
        <div class="carousel-item <?php echo ($key == 0 ? 'active': '') ?>">
        <img class="d-block w-100" src="<?php echo base_url().'/uploads/carousel/'.$rows['file_foto'] ?>" alt="First slide">
        <div class="carousel-caption d-none d-md-block">
            <h5><?php echo $rows['label'] ?></h5>
            <p><?php echo $rows['deskripsi'] ?></p>
        </div>
        </div>
        <?php endforeach; ?>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    </div>

    <div class="row mt-4">
        <?php foreach ($barang as $brg) : ?>
            <div class="card ml-3 mb-3" style="width: 18rem;">
            <img class="card-img-top" src="<?php echo base_url().'/uploads/'.$brg->gambar ?>" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title mb-1"><?php echo $brg->nama_brg?></h5>
                <p><b>Rp. <?php echo  number_format($brg->harga, 0,',','.')?></b></p>
                <small><?php echo $brg->deskripsi?></small><br>
                <?php echo anchor('dashboard/tambah_dikeranjang/'.$brg->id_brg,'<div class="btn btn-sm btn-primary">Tambah ke Keranjang</div>')?>
                <?php echo anchor('dashboard/detail/'.$brg->id_brg,'<div class="btn btn-sm btn-success">Detail</div>')?>
                
            </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
